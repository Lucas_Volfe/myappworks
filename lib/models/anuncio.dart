import 'package:http/http.dart';

class Anuncio {
  final int id;
  final String titulo;
  final String descricao;
  final String categoria_id;
  final List<int> fotos;
  final String bairro;
  final String localidade;
  final String cep;
  final double nota_avaliacao;
  final String comentario_avaliacao;
  final bool ativado;
  final bool aprovado;
  final int anunciante_id;
  final String created_at;

  Anuncio(
      this.id,
      this.titulo,
      this.descricao,
      this.categoria_id,
      this.fotos,
      this.bairro,
      this.localidade,
      this.cep,
      this.nota_avaliacao,
      this.comentario_avaliacao,
      this.ativado,
      this.aprovado,
      this.anunciante_id,
      this.created_at,
      ): assert(titulo != null);


  Anuncio.fromJson(Map<String, dynamic> json):
    id = json['id'],
    titulo = json['titulo'],
    descricao = json['descricao'],
    categoria_id = json['categoria_id'],
    fotos = json['fotos'],
    bairro = json['bairro'],
    localidade = json['localidade'],
    cep = json['cep'],
    nota_avaliacao = 0, //double.tryParse(json['nota_avaliacao']) ,//json['nota_avaliacao'].toDouble(),
    comentario_avaliacao = json['comentario_avaliacao'],
    ativado = json['ativado'],
    aprovado = json['aprovado'],
    anunciante_id = json['anunciante_id'].runtimeType == String ? int.tryParse(json['anunciante_id']):json['anunciante_id'],
    created_at = json['created_at'];

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'titulo': titulo,
        'descricao': descricao,
        'categoria_id': categoria_id,
        'fotos': fotos,
        'bairro': bairro,
        'localidade': localidade,
        'cep': cep,
        'nota_avaliacao': 0, // nota_avaliacao,
        'comentario_avaliacao': comentario_avaliacao,
        'ativado': ativado,
        'aprovado': aprovado,
        'anunciante_id': anunciante_id,
        'created_at' : created_at

      };

  @override
  String toString() {
    return 'Anuncio{id: $id, titulo: $titulo, descricao: $descricao, categoria_id: $categoria_id, fotos: $fotos, bairro: $bairro, localidade: $localidade, cep: $cep, nota_avaliacao: $nota_avaliacao, comentario_avaliacao: $comentario_avaliacao, ativado: $ativado, aprovado: $aprovado, anunciante_id: $anunciante_id, created_at: $created_at}';
  }
}
