class Usuario {
  final int id;
  final String email;
  final String username;
  final String password;
  final String cep;
  final String bairro;
  final String localidade;
  final int tipo_perfil;

  final String foto_perfil;

  Usuario.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        username = json['username'],
        password = json['password'],
        bairro = json['bairro'],
        localidade = json['localidade'],
        cep = json['cep'],
        tipo_perfil = json['tipo_perfil'],
        foto_perfil = json['foto_perfil'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': email,
        'username': username,
        'password': password,
        'bairro': bairro,
        'localidade': localidade,
        'cep': cep,
        'tipo_perfil': tipo_perfil,
        'foto_perfil': foto_perfil,
      };

  Usuario(
    this.id,
    this.email,
    this.username,
    this.password,
    this.cep,
    this.bairro,
    this.localidade,
    this.tipo_perfil,
    this.foto_perfil,
  );

  @override
  String toString() {
    return 'Usuario{id: $id, email: $email, username: $username, password: $password, cep: $cep, bairro: $bairro, localidade: $localidade, tipo_perfil: $tipo_perfil, foto_perfil: $foto_perfil}';
  }
}
