class Categoria {
  final int id;
  final String  nome;
  final String categoria;
  final String created_at;
  final String updated_at;

  Categoria(this.id, this.nome, this.categoria, this.created_at, this.updated_at);
}