import 'package:shared_preferences/shared_preferences.dart';

class UserStorage {

  void saveUser (jsonDecoded) async {

    print(jsonDecoded.runtimeType);
    SharedPreferences.setMockInitialValues({
      'access_token': jsonDecoded['access_token'],
      'id_usuario': jsonDecoded['usuario']['id'],
      'username': jsonDecoded['usuario']['username'],
      'cep': jsonDecoded['usuario']['cep'],
      'cidade': jsonDecoded['usuario']['cidade'],
      'bairro': jsonDecoded['usuario']['bairro'],
    });
  }
}