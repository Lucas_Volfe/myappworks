import 'package:myappworks/database/dao/anuncio_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dao/usuario_dao.dart';

Future<Database> getDatabase() async {
  final String path = join(await getDatabasesPath(), 'internal.db');
  return openDatabase(
    path,
    onCreate: (db, version) {
      db.execute(AnuncioDao.tableSql);
      db.execute(UsuarioDao.tableSql);
    },
    version: 1,
//      onDowngrade: onDatabaseDowngradeDelete,
  );
}

