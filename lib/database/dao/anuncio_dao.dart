import 'package:myappworks/models/anuncio.dart';
import 'package:sqflite/sqflite.dart';
import 'package:myappworks/database/app_database.dart';

class AnuncioDao {

  static const String tableSql = 'CREATE TABLE $_tableName ('
      '$_id INTEGER PRIMARY KEY, '
      '$_titulo TEXT, '
      '$_descricao TEXT, '
      '$_categoria INTEGER, '
      '$_fotos INTEGER)';

  static const String _tableName = 'anuncios';
  static const String _id = 'id';
  static const String _titulo = 'titulo';
  static const String _descricao = 'descricao';
  static const String _categoria = 'categoria';
  static const String _fotos = 'fotos';

  Future<int> save(Anuncio anuncio) async {
    final Database db = await getDatabase();
    Map<String, dynamic> anuncioMap = _toMap(anuncio);
    return db.insert(_tableName, anuncioMap);
  }

  Map<String, dynamic> _toMap(Anuncio anuncio) {
    final Map<String, dynamic> anuncioMap = Map();
    anuncioMap[_titulo] = anuncio.titulo;
    anuncioMap[_descricao] = anuncio.descricao;
    anuncioMap[_categoria] = anuncio.categoria_id;
    anuncioMap[_fotos] = anuncio.fotos;
    return anuncioMap;
  }

  Future<List<Anuncio>> findAll() async {
    final Database db = await getDatabase();
    final List<Map<String, dynamic>> result = await db.query(_tableName);
    List<Anuncio> anuncios = _toList(result);
    return anuncios;
  }

  List<Anuncio> _toList(List<Map<String, dynamic>> result) {
    final List<Anuncio> anuncios = List();
    for (Map<String, dynamic> row in result) {
//      final Anuncio anuncio = Anuncio(
//        row[_id],
//        row[_titulo],
//        row[_descricao],
//        row[_categoria],
//        row[_fotos],
//      );
//      anuncios.add(anuncio);
    }
    return anuncios;
  }

}