import 'dart:convert';
import 'package:http/http.dart';
import 'package:myappworks/http/webclient.dart';
import 'package:myappworks/models/anuncio.dart';
import 'package:myappworks/screens/cadastro_anuncio.dart';

class AnuncioWebClient {

  Future<List<Anuncio>> findAll() async {
    final Response response = await client.get(baseUrl + 'anuncios');
    List<Anuncio> anuncios = _toAnuncios(response);
    return anuncios;
  }

  Future<Anuncio> save(Anuncio anuncio) async {
    final String anuncioJson = jsonEncode(anuncio.toJson());
//    await Future.delayed(Duration(seconds: 5));
    final Response response = await client.post(
      baseUrl + 'anuncios',
      headers: {'Content-type': 'application/json'},
      body: anuncioJson,
    );

    if(response.statusCode == 201){
      return Anuncio.fromJson(jsonDecode(response.body));
    }
    throw HttpException(_getMessage(response.statusCode));
  }

  String _getMessage(int statusCode) {
    if(_statusCodeResponses.containsKey(statusCode)){
      return _statusCodeResponses[statusCode];
    }
  }

  static final Map<int, String> _statusCodeResponses = {
    400 : 'Ocorreu um erro ao enviar o anúncio',
    401 : 'Falha na autenticação'
  };

  List<Anuncio> _toAnuncios(Response response) {

    final decodedBody = jsonDecode(response.body);
    final List<Anuncio> anuncios = List();
    for (Map<String, dynamic> anuncioJson in decodedBody['data']) {
      anuncios.add(Anuncio.fromJson(anuncioJson));
    }
    return anuncios;
  }
}
