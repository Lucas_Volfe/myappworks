import 'dart:convert';
import 'package:http/http.dart';
import 'package:myappworks/http/webclient.dart';

class ViaCepWebClient {
  Future findCep(String cep) async {

//    await Future.delayed(Duration(seconds: 10));
    final Response response = await get(
      'https://viacep.com.br/ws/'+ cep +'/json/',
    );
    final decodedBody = jsonDecode(response.body);
    if(decodedBody['cep'] != null){
      return decodedBody;
    }
    return null;
  }
}