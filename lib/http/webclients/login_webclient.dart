import 'dart:convert';
import 'package:http/http.dart';
import 'package:myappworks/database/dao/usuario_dao.dart';
import 'package:myappworks/http/webclient.dart';
import 'package:myappworks/store_key_value/user_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginWebClient {

  Future<String> loginWithoutGoogle (credenciais) async {
    final String userJson = jsonEncode(credenciais);
    final _storage = UserStorage();
    final Response response = await client.post(
      baseUrl + 'login',
      headers: headers,
      body: userJson,
    );
    final jsonDecoded = jsonDecode(response.body);
    if(response.statusCode == 200){
      _storage.saveUser(jsonDecoded);
      return '200';
    }else{
      return '422';
    }
  }
}
