import 'dart:convert';
import 'package:http/http.dart';
import 'package:myappworks/http/webclient.dart';
import 'package:myappworks/models/usuario.dart';
import 'package:myappworks/screens/cadastro_anuncio.dart';

class UsuarioWebClient {

  Future<List<Usuario>> findAll() async {
    final Response response = await client.get(baseUrl + 'usuario');
    List<Usuario> usuarios = _toUsuario(response);
    return usuarios;
  }

  Future<Usuario> save(Usuario usuario) async {
    final String usuarioJson = jsonEncode(usuario.toJson());
    final Response response = await client.post(
      baseUrl + 'user',
      headers: headers,
      body: usuarioJson,
    );

    if(response.statusCode == 201){
      return Usuario.fromJson(jsonDecode(response.body));
    }
    throw HttpException(_getMessage(response.statusCode));
  }

  String _getMessage(int statusCode) {
    if(_statusCodeResponses.containsKey(statusCode)){
      return _statusCodeResponses[statusCode];
    }
  }

  static final Map<int, String> _statusCodeResponses = {
    400 : 'Ocorreu um erro ao enviar',
    401 : 'Falha na autenticação',
    500 : 'Ocorreu algum erro, contate o administrador'
  };

  List<Usuario> _toUsuario (Response response) {

    final decodedBody = jsonDecode(response.body);
    final List<Usuario> usuarios = List();
    for (Map<String, dynamic> anuncioJson in decodedBody['data']) {
      usuarios.add(Usuario.fromJson(anuncioJson));
    }
    return usuarios;
  }

  Future checkEmailExist(String email) async {
    final emailObj = {
      "email": email
    };
    final emailJson = jsonEncode(emailObj);
    final Response response = await client.post(
      baseUrl + 'check_email_exist',
      headers: headers,
      body: emailJson,
    );
    return response.body;
  }

  Future checkUsernameExist(String username) async {
    final usernameObj = {
      "username": username
    };
    final usernameJson = jsonEncode(usernameObj);
    final Response response = await client.post(
      baseUrl + 'check_username_exist',
      headers: headers,
      body: usernameJson,
    );
    return response.body;
  }
}
