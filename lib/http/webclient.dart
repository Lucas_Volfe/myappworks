import 'package:http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';

import 'interceptors/logging_interceptor.dart';

final Client client = HttpClientWithInterceptor.build(
  interceptors: [LoggingInterceptor()],
  requestTimeout: Duration(seconds: 5),
);

const String baseUrl = 'http://volfesolucoestecnologicas.com.br/APIs/encontre/public/api/';
//const String baseUrl = 'http://192.168.0.104:8000/api/';

const headers = {
  'Content-type': 'application/json'
};
