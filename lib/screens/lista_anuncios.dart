import 'package:flutter/material.dart';
import 'package:myappworks/http/webclients/anuncio_webclient.dart';
import 'package:myappworks/models/anuncio.dart';
import 'package:myappworks/widgets/centered_message.dart';
import 'package:myappworks/widgets/item_anuncio.dart';
import 'package:myappworks/widgets/progress.dart';

import 'cadastro_anuncio.dart';

class AdList extends StatefulWidget {
  @override
  _AdListState createState() => _AdListState();
}

class _AdListState extends State<AdList> {

  final AnuncioWebClient _webClient = AnuncioWebClient();

  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static const Color white = Colors.white;

  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final TextEditingController _filter = new TextEditingController();

//  final dio = new Dio(); // for http requests
  String _searchText = "";
  List names = new List(); // names we get from API
  List filteredNames = new List(); // names filtered by search text
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = Text('Anúncios');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _appBarTitle,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              icon: _searchIcon,
              onPressed: _searchPressed,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FutureBuilder<List<Anuncio>>(
          initialData: List(),
          future: _webClient.findAll(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                break;
              case ConnectionState.waiting:
                return Progress();
                break;
              case ConnectionState.active:
                break;
              case ConnectionState.done:
                if(snapshot.hasData){
                  final List<Anuncio> _anuncios = snapshot.data;
                  if (_anuncios.isNotEmpty) {
                    return ListView.builder(
                      itemCount: _anuncios.length,
                      itemBuilder: (context, index) {
                        return ItemAnuncio(_anuncios[index]);
                      },
                    );
                  }
                }
                return CenteredMessage(
                  'Nenhum anúncio encontrado',
                  icon: Icons.warning,
                );

                break;
            }
            return CenteredMessage('Unknown error');
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CadastroAnuncio(),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.location_city,
              color: white,
            ),
            title: Text('Região', style: TextStyle(color: white)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            title: Text('Categorias', style: TextStyle(color: white)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.filter_list),
            title: Text('Filtros'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          onEditingComplete: () {
            final String search = _filter.text;
            if (search.length > 0) {
              debugPrint(search);
            }
          },
          style: TextStyle(color: white),
          controller: _filter,
          decoration: new InputDecoration(
              hintText: 'Pesquisar...',
              hintStyle: TextStyle(color: white),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: white),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: white),
              )),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Anúncios');
        filteredNames = names;
        _filter.clear();
      }
    });
  }
}
