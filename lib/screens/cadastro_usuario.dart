import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:myappworks/http/webclients/login_webclient.dart';
import 'package:myappworks/http/webclients/usuario_webclient.dart';
import 'package:myappworks/http/webclients/via_cep_webclient.dart';
import 'package:myappworks/models/usuario.dart';
import 'package:myappworks/screens/login.dart';
import 'package:myappworks/store_key_value/user_storage.dart';
import 'package:myappworks/widgets/padding_divider.dart';
import 'package:myappworks/widgets/response_dialog.dart';
import 'package:myappworks/widgets/termos_condicoes.dart';
import 'package:url_launcher/url_launcher.dart';

import 'lista_anuncios.dart';

class CadastroUsuario extends StatefulWidget {
  final int _tipoPerfil;

  CadastroUsuario(this._tipoPerfil);

  @override
  _CadastroUsuarioState createState() {
    return _CadastroUsuarioState();
  }
}

class _CadastroUsuarioState extends State<CadastroUsuario> {
  final _usuarioWebClient = UsuarioWebClient();
  final _viaCep = ViaCepWebClient();
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _cepController = TextEditingController();
  String bairro = '';
  String localidade = '';
  var maskFormatter = MaskTextInputFormatter(
      mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
  var _hintUsername;
  var _hintEmail;
  var _hintCep;
  bool _buscandoCep = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Usuário'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 75),
        child: Form(
          key: _formKey,
          child: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.8,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: _usernameController,
                    decoration: InputDecoration(
                        labelText: 'Usuário', hintText: _hintUsername),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite um nome de usuário';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                        labelText: 'E-mail', hintText: _hintEmail),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite um e-mail valido';
                      }
                      return validateEmail(value);
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    decoration: InputDecoration(labelText: 'Senha'),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Digite uma senha valida';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    inputFormatters: [maskFormatter],
                    keyboardType: TextInputType.number,
                    decoration:
                        InputDecoration(labelText: 'CEP', hintText: _hintCep),
                    maxLength: 9,
                    controller: _cepController,
                    onChanged: _onCep,
                    validator: (value) {
                      if (value.isEmpty || value.length < 9) {
                        return 'Digite um CEP valido';
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: GestureDetector(
                      onTap: () async {
                        await launch(
                            'http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm');
                      },
                      child: Text(
                        'Procure seu CEP.',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: _buscandoCep,
                    child: Column(
                      children: <Widget>[
                        CircularProgressIndicator(),
                        Text('Buscando cep..'),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          bairro,
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          localidade,
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: RaisedButton(
                        onPressed: _save,
                        child: Text('Cadastre-se'),
                      ),
                    ),
                  ),
                  PaddingDivider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Já tem uma conta? ',
                        style: TextStyle(fontSize: 16),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => Login(),
                            ),
                          );
                        },
                        child: Text(
                          'Entrar',
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  TermosCondicoes(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _checkEmailExist(email) async {
    final response = await _usuarioWebClient.checkEmailExist(email);
    if (int.tryParse(response) > 0) {
      setState(() {
        _emailController.clear();
        _hintEmail = 'Email já cadastrado';
      });
    }
  }

  Future _checkUsernameExist(username) async {
    final response = await _usuarioWebClient.checkUsernameExist(username);
    if (int.tryParse(response) > 0) {
      setState(() {
        _usernameController.clear();
        _hintUsername = 'Usuário já cadastrado';
      });
    }
  }

  void _save() async {
    await _checkEmailExist(_emailController.text);
    await _checkUsernameExist(_usernameController.text);
    if (_formKey.currentState.validate() && localidade.length > 1) {
      final newUser = Usuario(
        null,
        _emailController.text,
        _usernameController.text,
        _passwordController.text,
        maskFormatter.getUnmaskedText(),
        bairro,
        localidade,
        widget._tipoPerfil,
        null,
      );
      showDialog(
          context: context,
          builder: (contextDialog) {
            return ProgressDialog(message: 'Enviando..');
          });
      Usuario usuario = await _send(newUser, context);
      _showSuccess(usuario, context, newUser.password);
    }
  }

  Future _showSuccess(Usuario usuario, BuildContext context, password) async {
    if (usuario != null) {
      final _loginWebClient = LoginWebClient();
      final credenciais = {"email": usuario.email, "password": password};
      final response = await _loginWebClient.loginWithoutGoogle(credenciais);
      if (response == '200') {
        final _storage = UserStorage();
        _storage.saveUser(usuario.toJson());
        await showDialog(
            context: context,
            builder: (contextDialog) {
              return SuccessDialog('Usuário criado com sucesso!');
            });
      }
    }
  }

  Future<Usuario> _send(Usuario novoUsuario, BuildContext context) async {
    final Usuario anuncio =
        await _usuarioWebClient.save(novoUsuario).catchError((e) {
      _showFailed(context, message: 'Tempo limite de conexão');
    }, test: (e) => e is TimeoutException).catchError((e) {
      _showFailed(context, message: 'Erro ao enviar usuário');
    }, test: (e) => e is HttpException).catchError((e) {
      _showFailed(context);
    }).whenComplete(() {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => AdList()),
          (Route<dynamic> route) => false);
    });
    return anuncio;
  }

  void _showFailed(BuildContext context, {String message = 'Unknown error'}) {
    showDialog(
        context: context,
        builder: (contextDialog) {
          return FailureDialog(message);
        });
  }

  void _onCep(text) async {
    if (text.length == 9) {
      setState(() => _buscandoCep = true);

      final result = await _viaCep.findCep(maskFormatter.getUnmaskedText());
      if (result != null) {
        setState(() {
          bairro = result['bairro'];
          localidade = result['localidade'];
          _buscandoCep = false;
        });
      } else {
        setState(() {
          _buscandoCep = false;
          _cepController.clear();
          bairro = '';
          localidade = '';
          maskFormatter = MaskTextInputFormatter(
              mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
          _hintCep = 'CEP invalido';
        });
      }
    }
  }

  String validateEmail(value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Digite um e-mail valido';
    else
      return null;
  }
}
