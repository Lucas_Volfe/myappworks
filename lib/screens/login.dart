import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:myappworks/screens/lista_anuncios.dart';
import 'package:myappworks/screens/seleciona_tipo_usuario.dart';
import 'package:myappworks/widgets/padding_divider.dart';
import 'package:myappworks/widgets/sing_in_google.dart';
import 'package:myappworks/widgets/termos_condicoes.dart';
import 'package:myappworks/http/webclients/login_webclient.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatelessWidget {
  final _loginWebClient = LoginWebClient();
  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final _formKey = GlobalKey<FormState>();


  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
//    googleSignIn.signOut();


    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 75),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                height: MediaQuery.of(context).size.width * 0.4,
                child: Image.asset(
                  'images/sh.jpeg',
                  fit: BoxFit.cover,
                ),
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _loginController,
                          decoration: InputDecoration(labelText: 'E-mail'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Digite um e-mail valido';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(labelText: 'Senha'),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Digite uma senha valida';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: RaisedButton(
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        final credenciais = {
                          "email": _loginController.text,
                          "password": _passwordController.text
                        };
                        final response = await _loginWebClient
                            .loginWithoutGoogle(credenciais);
                        if (response == '200') {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (context) => AdList()),
                              (Route<dynamic> route) => false);
                        } else {
                          showInSnackBar('Usuário ou senha inválido');
                        }
                      }
                    },
                    child: Text('Login'),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Text('Esqueçeu a senha?'),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.9,
                child: PaddingDivider(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Não tem uma conta? ',
                    style: TextStyle(fontSize: 16),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => SelecionaTipoUsuario(),
                        ),
                      );
                    },
                    child: Text(
                      'Cadastre-se',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                ],
              ),
              PaddingDivider(),
              SingInGoogle(),
              TermosCondicoes()
            ],
          ),
        ),
      ),
    );
  }

  _checkLogin(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final id = prefs.getInt('id_usuario') ?? null;
    if (id == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => AdList()),
              (Route<dynamic> route) => false);
    }
    return id;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          value,
          textAlign: TextAlign.center,
        )));
  }
}
