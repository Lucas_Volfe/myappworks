import 'dart:async';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:myappworks/http/webclients/anuncio_webclient.dart';
import 'package:myappworks/http/webclients/via_cep_webclient.dart';
import 'package:myappworks/models/anuncio.dart';
import 'package:myappworks/widgets/padding_divider.dart';
import 'package:myappworks/widgets/response_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class CadastroAnuncio extends StatefulWidget {
  @override
  _CadastroAnuncioState createState() => _CadastroAnuncioState();
}

class _CadastroAnuncioState extends State<CadastroAnuncio> {
  final _formKey = GlobalKey<FormState>();
  final AnuncioWebClient _webClient = AnuncioWebClient();
  String _categoria;
  List<String> _categorias = <String>[
    'Pedreiro',
    'Pintor',
    'Encanador',
    'Serviços Gerais',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
  ];

  final _tituloController = TextEditingController();
  final _descricaoController = TextEditingController();
  final _cepController = TextEditingController();
  final _viaCep = ViaCepWebClient();
  bool _buscandoCep = false;

  var _hintCep;
  var maskFormatter = MaskTextInputFormatter(
      mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
  int _categoriaController;
  String bairro = '';
  String localidade = '';
  int id_usuario;

  @override
  void dispose() {
    _tituloController.dispose();
    _descricaoController.dispose();
    super.dispose();
  }

  List<Asset> images = List<Asset>();
  String _error;

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    if (images.length > 0) {
      return GridView.count(
        crossAxisCount: 1,
        crossAxisSpacing: 4,
        mainAxisSpacing: 4,
        scrollDirection: Axis.horizontal,
        children: List.generate(images.length, (index) {
          Asset asset = images[index];
          return AssetThumb(
            asset: asset,
            width: 300,
            height: 300,
          );
        }),
      );
    }
  }

  Future<void> loadAssets() async {
    setState(() {
      images = List<Asset>();
    });

    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
          maxImages: 10,
          materialOptions: MaterialOptions(
            allViewTitle: "Inserir Imagens",
            actionBarColor: "#1b5e20",
            actionBarTitleColor: "#ffffff",
            selectionLimitReachedText: "Você não pode selecionar mais imagens.",
          ));
    } on Exception catch (e) {
//      error = e.message;
    }
    if (!mounted) return;
    if (resultList.length > 0) {
      setState(() {
        images = resultList;
        if (error == null) _error = 'No Error Dectected';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _checkLogin();
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Anuncio'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 8.0),
        child: Form(
          key: _formKey,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: loadAssets,
                  child: Container(
                    height: 110,
//                        color: Colors.grey,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('images/sh.jpeg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: buildGridView(),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: _tituloController,
                        decoration: InputDecoration(labelText: 'Titulo'),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Digite um titulo para o anuncio';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        maxLines: null,
                        keyboardType: TextInputType.multiline,
                        controller: _descricaoController,
                        decoration: InputDecoration(labelText: 'Descrição'),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Digite uma descrição para o anuncio';
                          }
                          return null;
                        },
                      ),

                      TextFormField(
                        inputFormatters: [maskFormatter],
                        maxLength: 9,
                        keyboardType: TextInputType.number,
                        controller: _cepController,
                        decoration: InputDecoration(
                            labelText: 'CEP', hintText: _hintCep),
                        onChanged: _onCep,
                        validator: (value) {
                          if (value.isEmpty || value.length < 9) {
                            return 'Digite um CEP valido';
                          }
                          return null;
                        },
                      ),
                      Visibility(
                        visible: _buscandoCep,
                        child: Column(
                          children: <Widget>[
                            CircularProgressIndicator(),
                            Text('Buscando cep..'),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              bairro,
                              style: TextStyle(fontSize: 16),
                            ),
                            Text(
                              localidade,
                              style: TextStyle(fontSize: 16),
                            ),
                          ],
                        ),
                      ), //
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: DropdownButton<String>(
                          isExpanded: true,
                          items: _categorias.map((String value) {
                            return DropdownMenuItem(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                          onChanged: (String value) {
                            setState(() {
                              _categoria = value;
                              _categoriaController = _categorias.indexOf(value);
                            });
                          },
                          hint: Text('Selecione a categoria'),
                          value: _categoria,
                        ),
                      ),

                      PaddingDivider(),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: RaisedButton(
                          onPressed: () {
                            _save(context);

//                            if (_formKey.currentState.validate() && localidade.length > 0) {
//                              _save(context);
//                            }
                          },
                          child: Text('Criar anuncio'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _save(BuildContext context) async {
//    ByteData byteData = await images[0].getByteData();
//    List<int> imageData = byteData.buffer.asUint8List();

    final Anuncio novoAnuncio = Anuncio(
      null,
      _tituloController.text,
      _descricaoController.text,
      _categoriaController.toString(),
      null,
      bairro,
      localidade,
      maskFormatter.getUnmaskedText(),
      5,
      'comentario_avaliacao',
      false,
      false,
      id_usuario,
      null,
    );

//    var i = {
//      "anuncio_id": 88,
//      "foto" : "$imageData"
//    };
//    _webClient.saveFoto(i);

    showDialog(
        context: context,
        builder: (contextDialog) {
          return ProgressDialog(message: 'Enviando..');
        });

    Anuncio anuncio = await _send(novoAnuncio, context);
    _showSuccess(anuncio, context);
  }

  Future<Anuncio> _send(Anuncio novoAnuncio, BuildContext context) async {
    final Anuncio anuncio = await _webClient.save(novoAnuncio).catchError((e) {
      _showFailed(context, message: 'Tempo limite de conexão');
    }, test: (e) => e is TimeoutException).catchError((e) {
      _showFailed(context, message: 'Erro ao enviar anúncio');
    }, test: (e) => e is HttpException).catchError((e) {
      print('3');
      _showFailed(context);
    }).whenComplete(() {
      Navigator.pop(context);
    });
    return anuncio;
  }

  _checkLogin() async {
    final prefs = await SharedPreferences.getInstance();
    final id = prefs.getInt('id_usuario') ?? null;
    if (id == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Login()),
          (Route<dynamic> route) => false);
    }else{
      id_usuario = id;
    }
  }

  Future _showSuccess(Anuncio anuncio, BuildContext context) async {
    if (anuncio != null) {
      await showDialog(
          context: context,
          builder: (contextDialog) {
            return SuccessDialog('Anúncio criado com sucesso!');
          });
      Navigator.pop(context);
    }
  }

  void _showFailed(BuildContext context, {String message = 'Unknown error'}) {
    showDialog(
        context: context,
        builder: (contextDialog) {
          return FailureDialog(message);
        });
  }

  void _onCep(text) async {
    if (text.length == 9) {
      setState(() => _buscandoCep = true);
      final result = await _viaCep.findCep(maskFormatter.getUnmaskedText());
      if (result != null) {
        setState(() {
          bairro = result['bairro'];
          localidade = result['localidade'];
          _buscandoCep = false;
        });
      } else {
        setState(() {
          _cepController.clear();
          _buscandoCep = false;
          bairro = '';
          localidade = '';
          maskFormatter = MaskTextInputFormatter(
              mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
          _hintCep = 'CEP invalido';
        });
      }
    }
  }
}

class HttpException implements Exception {
  final String message;

  HttpException(this.message);
}
