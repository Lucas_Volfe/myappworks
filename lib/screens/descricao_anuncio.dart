import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:myappworks/models/anuncio.dart';
import 'package:myappworks/widgets/padding_divider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';




class DescricaoAnuncio extends StatelessWidget {
  final Anuncio _anuncio;
  DescricaoAnuncio(this._anuncio);

  var textEditingController = TextEditingController(text: "12345678");
  var maskFormatter = MaskTextInputFormatter(
      mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});



  @override
  Widget build(BuildContext context) {
    final DateTime created = DateTime.parse(_anuncio.created_at);
    final dM = DateFormat.MMMMd().format(created);
    final hour = DateFormat.Hm().format(created);
    final formatCep = _anuncio.cep.substring(0,5) +'-'+_anuncio.cep.substring(5);

    return Scaffold(
      appBar: AppBar(
        title: Text('Descrição'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: PhotoView(
                imageProvider: AssetImage('images/sh.jpeg'),
              )),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  _anuncio.titulo,
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Text(

                    'Públicado em $dM às $hour',
                    style: TextStyle(fontSize: 14, color: Colors.grey),
                  ),
                ),
                PaddingDivider(),
                Text(
                  'Descrição',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                TextPadding(_anuncio.descricao),
                PaddingDivider(),
                Text(
                  'Detalhes',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextPadding('Categoria'),
                    TextPadding(_anuncio.categoria_id.toString()),
                  ],
                ),
                PaddingDivider(),
                Text(
                  'Localização',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextPadding('CEP'),
                    TextPadding(formatCep),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextPadding('Município'),
                    TextPadding(_anuncio.localidade),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TextPadding('Bairro'),
                    TextPadding(_anuncio.bairro),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TextPadding extends StatelessWidget {
  final String _text;
  TextPadding(this._text);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Text(
        _text,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
