import 'package:flutter/material.dart';
import 'package:myappworks/screens/cadastro_usuario.dart';

class SelecionaTipoUsuario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cadastro Usuario'),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/sh.jpeg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CadastroUsuario(1),
                    ),
                  );
                },
                child: Text('Está procurando serviço'),
              ),
              RaisedButton(
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CadastroUsuario(2),
                    ),
                  );
                },
                child: Text('Está disponibilizando serviço'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
