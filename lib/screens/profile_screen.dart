import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:myappworks/models/user_details_firebase.dart';
import 'package:myappworks/screens/login.dart';



class ProfileScreen extends StatelessWidget {
  final UserDetails detailsUser;

  ProfileScreen({Key key, @required this.detailsUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GoogleSignIn _gSignIn =  GoogleSignIn();

    return  Scaffold(
        appBar:  AppBar(
          title:  Text(detailsUser.userName),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.exit_to_app,
                size: 20.0,
                color: Colors.white,
              ),
              onPressed: () async {
                await _gSignIn.signOut();
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) {return Login();}), ModalRoute.withName('/'));
              },
            ),
          ],
        ),
        body:Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              backgroundImage:NetworkImage(detailsUser.photoUrl),
              radius: 50.0,
            ),
            SizedBox(height:10.0),
            Text(
              "Name : " + detailsUser.userName,
              style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.black,fontSize: 20.0),
            ),
            SizedBox(height:10.0),
            Text(
              "Email : " + detailsUser.userEmail,
              style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.black,fontSize: 20.0),
            ),
            SizedBox(height:10.0),
            Text(
              "Provider : " + detailsUser.providerDetails,
              style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.black,fontSize: 20.0),
            ),
          ],
        ),)
    );
  }
}