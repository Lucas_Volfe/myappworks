import 'package:flutter/material.dart';

class TermosCondicoes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.9,
        child: Card(
          color: Colors.white70,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Ao continuar, você concorda com os Termos de Uso e a Política de Privacidade da APPWORKS, e também, em receber comunicações via e-mail e push da APPWORKS e seus parceiros.',
              style: TextStyle(color: Colors.black54),
            ),
          ),
        ),
      ),
    );
  }
}
