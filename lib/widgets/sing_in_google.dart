import 'dart:async';
import 'dart:ffi';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:myappworks/http/webclients/login_webclient.dart';
import 'package:myappworks/http/webclients/usuario_webclient.dart';
import 'package:myappworks/http/webclients/via_cep_webclient.dart';
import 'package:myappworks/models/user_details_firebase.dart';
import 'package:myappworks/models/usuario.dart';
import 'package:myappworks/screens/lista_anuncios.dart';
import 'package:myappworks/store_key_value/user_storage.dart';
import 'package:myappworks/widgets/response_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class SingInGoogle extends StatefulWidget {
  @override
  _SingInGoogleState createState() => _SingInGoogleState();
}

class _SingInGoogleState extends State<SingInGoogle> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  String name;
  String email;
  String imageUrl;
  final _usuarioWebClient = UsuarioWebClient();

  Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    showDialog(
      context: context,
      builder: (contextDialog) {
        return ProgressDialog(
          message: 'Carregando..',
        );
      },
    );

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    name = user.displayName;
    email = user.email;
    imageUrl = user.photoUrl;

    final checkEmail = await _usuarioWebClient.checkEmailExist(email);
    if (checkEmail == '0') {
      if (name.contains(" ")) {
        name = name.substring(0, name.indexOf(" "));
      }

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final FirebaseUser currentUser = await _auth.currentUser();
      assert(user.uid == currentUser.uid);

      UserDetails details = new UserDetails(
        user.providerId,
        user.displayName,
        user.photoUrl,
        user.email,
      );

      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (contextDialog) {
            return TerminarCadastro(details);
          });
      return 'signInWithGoogle succeeded: $user';
    } else {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (contextDialog) {
            return FailureDialog('Usuário já cadastrado');
          });
    }
  }

  void signOutGoogle() async {
    await googleSignIn.signOut();
    print("User Sign Out");
  }

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () {
        signInWithGoogle();
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.grey),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage("images/google_logo.png"), height: 35.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'Entre com uma conta Google',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TerminarCadastro extends StatefulWidget {
  final UserDetails _details;

  TerminarCadastro(this._details);

  @override
  _TerminarCadastroState createState() => _TerminarCadastroState();
}

class _TerminarCadastroState extends State<TerminarCadastro> {
  int currentStep = 0;
  bool complete = false;
  var _hintCep;
  final _cepController = TextEditingController();
  static MaskTextInputFormatter maskFormatter = MaskTextInputFormatter(
      mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
  String bairro = '';
  String localidade = '';
  final _viaCep = ViaCepWebClient();
  final _formKey = GlobalKey<FormState>();
  int _radioValue = 2;
  final _usuarioWebClient = UsuarioWebClient();
  bool _buscandoCep = false;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 24.0),
                child: Text(
                  'Termine seu cadastro',
                  style: TextStyle(fontSize: 24),
                ),
              ),
              SizedBox(
                child: Container(
                  child: Form(
                    key: _formKey,
                    child: Stepper(
                      steps: [
                        Step(
                          state: StepState.editing,
                          isActive: true,
                          title: Text(
                            "Selecione o tipo de perfil",
                            style: TextStyle(fontSize: 16),
                          ),
                          content: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('Prestador de serviços'),
                                  Radio(
                                    value: 1,
                                    groupValue: _radioValue,
                                    onChanged: (int i) =>
                                        setState(() => _radioValue = i),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text('Procurando prestador'),
                                  Radio(
                                    value: 2,
                                    groupValue: _radioValue,
                                    onChanged: (int i) =>
                                        setState(() => _radioValue = i),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Step(
                          title: Text("Digite seu CEP"),
                          isActive: false,
                          state: StepState.editing,
                          content: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextFormField(
                                inputFormatters: [maskFormatter],
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    labelText: 'CEP', hintText: _hintCep),
                                maxLength: 9,
                                controller: _cepController,
                                onChanged: _onCep,
                                validator: (value) {
                                  if (value.isEmpty || value.length < 9) {
                                    return 'Digite um CEP valido';
                                  }
                                  return null;
                                },
                              ),
                              Visibility(
                                visible: true,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      bairro,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                    Text(
                                      localidade,
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: GestureDetector(
                                  onTap: () async {
                                    await launch(
                                        'http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCepEndereco.cfm');
                                  },
                                  child: Text(
                                    'Procure seu CEP.',
                                    style: TextStyle(
                                        color: Colors.blue,
                                        decoration: TextDecoration.underline),
                                  ),
                                ),
                              ),
                              Visibility(
                                visible: _buscandoCep,
                                child: Column(
                                  children: <Widget>[
                                    CircularProgressIndicator(),
                                    Text('Buscando cep..'),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                      currentStep: currentStep,
                      onStepContinue: next,
                      onStepTapped: (step) => goTo(step),
                      onStepCancel: cancel,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  next() async {
    if (currentStep + 1 != 2) {
      goTo(currentStep + 1);
    } else {
      setState(() {
        return complete = true;
      });

      if (_formKey.currentState.validate() && localidade.length > 1) {
        final newUser = Usuario(
            null,
            widget._details.userEmail,
            widget._details.userName,
            'google',
            maskFormatter.getUnmaskedText(),
            bairro,
            localidade,
            _radioValue,
            null);
        Usuario usuario = await _send(newUser, context);
        _showSuccess(usuario, context);
      }
    }
  }

  Future _showSuccess(Usuario usuario, BuildContext context) async {
    if (usuario != null) {

      final _loginWebClient = LoginWebClient();
      final credenciais = {
        "email": usuario.email,
        "password": 'google'
      };
      final response = await _loginWebClient.loginWithoutGoogle(credenciais);
      if (response == '200') {
        final _storage = UserStorage();
        _storage.saveUser(usuario.toJson());
        await showDialog(
            context: context,
            builder: (contextDialog) {
              return SuccessDialog('Usuário criado com sucesso!');
            });
      }
    }
  }

  Future<Usuario> _send(Usuario novoUsuario, BuildContext context) async {
    final Usuario anuncio =
        await _usuarioWebClient.save(novoUsuario).catchError((e) {
      _showFailed(context, message: 'Tempo limite de conexão');
    }, test: (e) => e is TimeoutException).catchError((e) {
      _showFailed(context, message: 'Erro ao enviar usuário');
    }, test: (e) => e is HttpException).catchError((e) {
      _showFailed(context);
    }).whenComplete(() {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => AdList()),
          (Route<dynamic> route) => false);
    });
    return anuncio;
  }

  void _showFailed(BuildContext context, {String message = 'Unknown error'}) {
    showDialog(
        context: context,
        builder: (contextDialog) {
          return FailureDialog(message);
        });
  }

  cancel() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }

  goTo(int step) {
    setState(() => currentStep = step);
  }

  _onCep(text) async {
    if (text.length == 9) {
      setState(() => _buscandoCep = true);
      final result = await _viaCep.findCep(maskFormatter.getUnmaskedText());
      if (result != null) {
        setState(() {
          bairro = result['bairro'];
          localidade = result['localidade'];
          _buscandoCep = false;
        });
        return true;
      }
      setState(() {
        _buscandoCep = false;
        _cepController.clear();
        bairro = '';
        localidade = '';
        maskFormatter = MaskTextInputFormatter(
            mask: '#####-###', filter: {"#": RegExp(r'[0-9]')});
        _hintCep = 'CEP invalido';
      });
    }
  }
}
