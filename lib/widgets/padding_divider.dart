import 'package:flutter/material.dart';

class PaddingDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only (top: 16.0, bottom: 16.0),
      child: Divider(),
    );
  }
}