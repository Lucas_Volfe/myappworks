import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:myappworks/screens/login.dart';

void main(){
  testWidgets('Deve apresentar a logo da pagina de login', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(home: Login()));
    final logoImage = find.byType(Image);
    expect(logoImage, findsWidgets);
  });
}