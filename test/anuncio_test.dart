import 'package:flutter_test/flutter_test.dart';
import 'package:myappworks/models/anuncio.dart';

void main(){
  test('Deve retornar o anuncio apos ser criado', (){
    final anuncio = Anuncio(
      null,
      'titulo',
      'descricaoController.text',
      '1',
      null,
      'bairro',
      'localidade',
      'cep',
      5,
      'comentario_avaliacao',
      false,
      false,
      1,
      null,
    );
    expect(anuncio.titulo, 'titulo');
  });

  test('Deve apresentar o erro ao criar um transferecia sem titulo', (){
    expect(()=>Anuncio(
      null,
      'titulo',
      'descricaoController.text',
      '1',
      null,
      'bairro',
      'localidade',
      'cep',
      5,
      'comentario_avaliacao',
      false,
      false,
      1,
      null,
    ), throwsAssertionError);
  });
  
}